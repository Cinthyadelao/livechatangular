const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http, {
    cors: {
        origin: true,
        credentials: true,
        methods: ["GET", "POST"]
    }
});

io.on('connection', (socket) => { //--event
    console.log('New user connected');
    socket.on("sendMessage", (messageInfo) => {
        console.log("message sent");
        socket.broadcast.emit("receiveMessage", messageInfo); // tous les utilisateurs connectés reçoivent le message sauf l'expéditeur.
    });
});

/* TEST
app.get('/', (req, res) => {
    res.send('<h1>Hola</h1>');
});*/

http.listen(3000, () => {
    console.log('Listening in port  3000');
})